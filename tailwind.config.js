const plugin = require("tailwindcss/plugin");

/**
 * Tailwind js configuration
 *
 * allow to add and manipulate, customize the css properties, colors, theme and more
 * add some flexibility to the project
 */
module.exports = {
  // content should be used for the css properties will be generated
  content: ["./app/**/*.{js,ts,jsx,tsx}"],

  // theme the application and allow to customize some properties and colors
  theme: {
    // add font family
    // add some typography for special purpose like body, heading, and writing
    fontFamily: {
      body: ["Dm Sans", "sans-serif"],
      heading: ["Poppins", "sans-serif"],
      writing: ["Leckerli One", "cursive"],
    },

    // extends some element without replace it
    // by default
    extend: {
      // colors extends
      colors: {
        "primary-background": "#FCFCFC",
        primary: "#253BFF",
      },
    },
  },

  // plugins shoud be add and use
  plugins: [
    // add some plugins to add some utilities
    plugin(function ({ addUtilities }) {
      // add some element using the utilities classes
      // utility class to hide the scrollbar
      addUtilities({
        ".no-scroll": {
          "-ms-overflow-style": "none",
          "scrollbar-width": "none",
        },

        ".no-scroll::-webkit-scrollbar": {
          display: "none",
        },
      });
    }),
  ],
};
