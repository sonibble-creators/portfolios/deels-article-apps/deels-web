import { Icon } from "@iconify/react";

/**
 *
 * # Profile
 *
 * Allow to show the profile, and some information about the users
 *
 * @returns JSX.Element
 */
export default function Profile(): JSX.Element {
  return (
    <main className="flex flex-col">
      {/* profile section */}
      {/* show some summary data and information about the user */}
      <section className="flex flex-col">
        <div className="flex bg-white bg-opacity-50 backdrop-blur-3xl border-2 border-white p-3 rounded-3xl flex-col items-center">
          {/* cover of profile users and the readers */}
          <div className="flex overflow-hidden rounded-2xl h-[340px] w-full">
            <img
              src="https://images.unsplash.com/photo-1553356084-58ef4a67b2a7?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1887&q=80"
              alt=""
              className="object-cover h-full w-full"
            />
          </div>

          {/* profile photo */}
          <div className="flex justify-center items-center h-24 w-24 rounded-3xl border-2 border-white overflow-hidden -mt-10">
            <img
              src="https://images.unsplash.com/photo-1553356084-58ef4a67b2a7?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1887&q=80"
              alt=""
              className="object-cover h-full w-full"
            />
          </div>

          {/* the name and some bio for the users */}
          <div className="flex flex-col items-center mt-5 w-full gap-4 mb-5">
            <h4 className="text-black font-semibold font-heading text-xl text-center leading-normal">
              Jordan Layler
            </h4>
            <span className="text-center leading-relxed text-gray-600 w-5/12">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam
              nisi eos corrupti laudantium nobis nulla! Veritatis ex at
              voluptatem officiis consequuntur magnam nemo eum libero, sapiente
              sed. Eaque, necessitatibus quae?
            </span>
          </div>
        </div>
      </section>

      {/* some actions and summary */}
      <section className="flex flex-col mt-20">
        {/* the summary  */}
        <div className="flex gap-12">
          {/* the summary */}
          <div className="flex p-5 border-2 border-white rounded-3xl bg-white bg-opacity-50 gap-5 w-1/3">
            <div className="flex flex-col flex-1 items-center gap-2">
              <span className="font-heading font-semibold text-2xl text-black">
                25
              </span>
              <span className="text-gray-600 text-center leading-snug text-sm">
                Article Readed
              </span>
            </div>
            <div className="flex flex-col flex-1 items-center gap-2">
              <span className="font-heading font-semibold text-2xl text-black">
                100K
              </span>
              <span className="text-gray-600 text-center leading-snug text-sm">
                Likes
              </span>
            </div>
            <div className="flex flex-col flex-1 items-center gap-2">
              <span className="font-heading font-semibold text-2xl text-black">
                200
              </span>
              <span className="text-gray-600 text-center leading-snug text-sm">
                Articles
              </span>
            </div>
          </div>

          {/* the actions */}
          {/* some action to the users like logout and edit profile */}
          <div className="flex w-1/3 flex-col gap-5">
            {/* Edit Profile out action */}
            <div className="flex p-3 rounded-3xl border-2 border-white bg-white bg-opacity-50 backdrop-blur-3xl gap-4 items-center transition-all duration-300 hover:-translate-y-3">
              {/* icons */}
              <div className="flex h-14 w-14 rounded-2xl bg-primary bg-opacity-10 justify-center items-center">
                <Icon
                  icon="iconoir:profile-circled"
                  className="h-6 w-6 text-primary"
                />
              </div>

              {/* texts */}
              <div className="flex flex-col gap-1 grow">
                <span className="font-bold text-black">Edit Profile</span>
                <span className="text-gray-500 text-sm">
                  Edit and change profile
                </span>
              </div>

              {/* the arrow */}
              <div className="flex justify-center items-center h-10 w-10 rounded-2xl">
                <Icon
                  icon="akar-icons:chevron-right"
                  className="h-6 w-6 text-gray-500"
                />
              </div>
            </div>

            {/* log out action */}
            <div className="flex p-3 rounded-3xl border-2 border-white bg-white bg-opacity-50 backdrop-blur-3xl gap-4 items-center transition-all duration-300 hover:-translate-y-3">
              {/* icons */}
              <div className="flex h-14 w-14 rounded-2xl bg-primary bg-opacity-10 justify-center items-center">
                <Icon
                  icon="iconoir:profile-circled"
                  className="h-6 w-6 text-primary"
                />
              </div>

              {/* texts */}
              <div className="flex flex-col gap-1 grow">
                <span className="font-bold text-black">Log out</span>
                <span className="text-gray-500 text-sm">
                  Browse without account
                </span>
              </div>

              {/* the arrow */}
              <div className="flex justify-center items-center h-10 w-10 rounded-2xl">
                <Icon
                  icon="akar-icons:chevron-right"
                  className="h-6 w-6 text-gray-500"
                />
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  );
}
