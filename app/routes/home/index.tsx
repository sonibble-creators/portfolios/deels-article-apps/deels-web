import ArticleItemCard from "~/components/cards/article-item-card";
import PopularArticleItemCard from "~/components/cards/popular-article-item-card";

/**
 * # Feed
 *
 * the feed allow to show some and such as the article that fit and based on your preferences
 *
 * @returns JSX.Element
 */
export default function Feed(): JSX.Element {
  return (
    <main className="flex flex-col">
      {/* popular article sections */}
      <section className="flex flex-col gap-12">
        {/* heading */}
        <div className="flex">
          <h3 className="font-semibold text-black text-3xl font-heading">
            Popular this week
          </h3>
        </div>

        {/* all content */}
        {/* show the popular content for simple view to users */}
        <div className="flex gap-6">
          {[1, 2, 3, 4].map((index: number) => (
            <PopularArticleItemCard key={index} />
          ))}
        </div>
      </section>

      {/* sort for you content */}
      <section className="flex flex-col gap-12 mt-24">
        {/* heading */}
        <div className="flex">
          <h3 className="font-semibold text-black text-3xl font-heading">
            Sort for you
          </h3>
        </div>

        {/* all content */}
        {/* show the popular content for simple view to users */}
        <div className="grid grid-cols-2 gap-6">
          {[1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9].map(
            (index: number) => (
              <ArticleItemCard key={index} />
            )
          )}
        </div>
      </section>
    </main>
  );
}
