import ArticleItemCard from "~/components/cards/article-item-card";

/**
 * # Explore
 *
 * Show the and exlore the content about the article
 *
 * @returns JSX.Element
 */
export default function Explore(): JSX.Element {
  return (
    <main className="flex flex-col">
      {/* All explore content */}
      <section className="flex flex-col gap-12 mt-24">
        {/* all content */}
        {/* show the popular content for simple view to users */}
        <div className="grid grid-cols-2 gap-6">
          {[1, 2, 3, 4, 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, 6, 7, 8, 9].map(
            (index: number) => (
              <ArticleItemCard key={index} />
            )
          )}
        </div>
      </section>
    </main>
  );
}
