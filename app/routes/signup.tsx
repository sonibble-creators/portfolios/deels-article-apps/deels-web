import { MetaFunction } from "@remix-run/node";
import { Form } from "@remix-run/react";
import { AnimatePresence } from "framer-motion";
import { useEffect, useState } from "react";
import welcomeImage from "~/assets/images/illustration/welcom.png";
import UserService from "~/logic/service/user-service";

/**
 * # meta
 *
 * the meta tags for seo purpose
 *
 * @returns JSX.Element
 */
export const meta: MetaFunction = () => ({
  title: "Sign Up - Become member of thousand writer and change live",
  description: "Sign up to thousand writer and change live",
  keywords:
    "sign up, thousand writer, thousand writer sign up, thousand writer sign up page, thousand writer sign up page",
});

// predefine the service
// bind some service
const userService: UserService = new UserService();

/**
 * # Signup
 *
 * signin component page to sign and enable user to manage the account
 * sign enable user to signup
 *
 * @returns JSX.Element
 */
export default function SignIn(): JSX.Element {
  // error
  // enable to show some error when signup the new user
  // something wrong happen, so we can handle all of the error
  const [error, setError] = useState<string | undefined>(undefined);

  // onSignUpwithGoogle
  // handle all event when signup a new user using google provider
  // will show some popup to take the user to become the members
  const onSignUpWithGoogle: () => Promise<void> = async () => {
    // firts we need to handle the event into the service
    const [error, data] = await userService.signUpWithGoogle();

    // check the data and error message
    if (error) {
      // opps something error found
      // we need to show the error message
      setError(error);
    }

    // nice, all of the workflow are going well
    setError(undefined);

    // and then bring it into the home
    if (data) {
      // when data is found, we're going to move it into the home page
      // location.href = "/home";
    }
  };

  // checkAccountExist
  // enable to check the account and redirect when the account already found
  const checkAccountExist: () => Promise<void> = async () => {
    // we need to get data from the user store
    const result = await userService.checkAccountExists();

    // check and validate
    if (result) {
      // nice we need to jump into home
      // location.href = "/home";
    }
  };

  // use effect for single call
  useEffect(() => {
    // check availability of current user
    checkAccountExist();

    // also load the data
    userService.loadUser();
  }, []);

  return (
    <div>
      {/* header */}
      {/* header place to here */}

      {/* main body */}
      {/* add all of the body content to the website application */}
      <main className="flex flex-col">
        {/* section  */}
        {/* show all of the introduction and some information about the website */}
        <section className="flex container mx-auto h-screen gap-20">
          {/* left side contain some illustration and images */}
          <div className="flex w-1/2 overflow-hidden">
            <img src={welcomeImage} alt="" className="object-fill" />
          </div>

          {/* right side about the application or website */}
          <div className="flex flex-col w-1/2 justify-center">
            {/* leading and desc */}
            <div className="flex flex-col gap-11">
              <span className="text-pink-500 font-semibold font-heading text-lg">
                Sign up .
              </span>
              <h2 className="text-black font-bold text-5xl leading-tight font-heading">
                Become member of creative people and connect around the world
              </h2>
              <span className="text-xl text-gray-700 font-body">
                Become more creative and feel free to grow your mindset with the
                brilliant article around the world
              </span>

              {/* the form */}
              {/* form to sign up to the website and used other features */}
              <Form className="flex flex-col w-8/12 gap-6 mt-8">
                {/* Fullname */}
                <div className="flex flex-col group gap-2">
                  <input
                    type="text"
                    placeholder="Your full name"
                    name="fullname"
                    className="ring-0 outline-none bg-transparent w-full border-2 border-gray-200 h-12 rounded-2xl px-4 flex items-center focus:border-pink-500 transition-all duration-300 hover:-translate-x-2"
                  />
                </div>

                {/* email address */}
                <div className="flex flex-col group gap-2">
                  <input
                    type="email"
                    placeholder="Your email address"
                    name="email"
                    className="ring-0 outline-none bg-transparent w-full border-2 border-gray-200 h-12 rounded-2xl px-4 flex items-center focus:border-pink-500 transition-all duration-300 hover:-translate-x-2"
                  />
                </div>

                {/* password */}
                <div className="flex flex-col group gap-2">
                  <input
                    type="password"
                    placeholder="Your password"
                    name="password"
                    className="ring-0 outline-none bg-transparent w-full border-2 border-gray-200 h-12 rounded-2xl px-4 flex items-center focus:border-pink-500 transition-all duration-300 hover:-translate-x-2"
                  />
                </div>

                {/* message when something error found */}
                {/* maybe when the email is not recognize, password gonna be wrong */}
                {/* only show when some throuble found when sign up */}
                <AnimatePresence>
                  {error && (
                    <div className="flex">
                      <div className="flex p-3 rounded-xl bg-red-100 text-red-500 text-sm">
                        Opps your password wrong. Please use the correct
                        password
                      </div>
                    </div>
                  )}
                </AnimatePresence>

                {/* submit button */}
                <div className="flex mt-4">
                  <button
                    type="submit"
                    className="text-white bg-pink-500 rounded-2xl w-full h-12 transition-all duration-500 hover:-translate-y-3 hover:scale-110 shadow-2xl shadow-pink-200"
                  >
                    Sign Up
                  </button>
                </div>
              </Form>

              {/* or using other method to sign up  */}
              {/* enable user to use other method to sign in like google, facebook, etc */}
              <div className="flex flex-col w-8/12">
                <span className="text-gray-500 text-center">
                  or sign up using
                </span>

                {/* Google Sign in */}
                <div className="flex mt-5">
                  <button
                    className="text-violet-500 bg-violet-50 rounded-2xl w-full h-12 plx-10 transition-all duration-500 hover:-translate-y-3 hover:scale-110"
                    onClick={() => onSignUpWithGoogle()}
                  >
                    Sign up with Google
                  </button>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>

      {/* footer */}
      {/* add some footer to the page */}
    </div>
  );
}
