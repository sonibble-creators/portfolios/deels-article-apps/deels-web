import { Outlet } from "@remix-run/react";
import MainHeader from "~/components/header/main-header";
import SideNav from "~/components/navigation/side-nav";

export default function Home(): JSX.Element {
  return (
    <div>
      {/* background with gradient and bluring */}
      <div className="">
        {/* circle one */}
        <div className="flex h-[320px] w-[320px] rounded-full bg-[#FF29EA] fixed left-40 top-40 blur-[320px]"></div>
        <div className="flex h-[320px] w-[320px] rounded-full bg-primary fixed right-40 bottom-40 blur-[320px]"></div>
      </div>

      {/* Header place */}
      {/* header enable add some basic info about the user, search and more */}
      <MainHeader />

      {/* navigation */}
      {/* add some navigation like side nav, and other */}
      <SideNav />

      {/* main body */}
      {/* the nested layouy and become the child for the home */}
      <div className="absolute inset-0 w-[70%] ml-[320px] mt-[133px]">
        <Outlet />
      </div>

      {/* footer  */}
    </div>
  );
}
