import { useEffect } from "react";
import welcomeImage from "~/assets/images/illustration/welcom.png";
import SettingService from "~/logic/service/setting-service";

// bind some service
const settingService = new SettingService();

/**
 * # Welcome
 *
 * welcome screen page for showing the welcome and some introduction
 * for the user who want to use the application
 *
 *
 * @returns JSX.Element
 */
export default function Welcome(): JSX.Element {
  // get started clicked
  // handle the get started button was clikcked
  // add some event and redirect to the home page
  const onGetStartedClicked: () => void = () => {
    // add some local storage to save the get started was clicked
    // and passed
    settingService.setGetStartedPassed();

    // change some location
    // and now we need to redirect to other home
    setTimeout(() => {
      location.href = "/home";
    }, 1200);
  };

  // check if the user is already been passed the get started and then we bring it to home
  const checkGetStartedPassed: () => void = async () => {
    // we need to get the record from the local storage
    const result: boolean = await settingService.isAlreadyGetStartedPassed();

    // check and validate
    if (result) {
      // ok, the users is already has been passed
      // change some location
      // and now we need to redirect to other home
      setTimeout(() => {
        location.href = "/home";
      }, 1200);
    }
  };

  // use effect for single call
  useEffect(() => {
    // check the get started passed
    checkGetStartedPassed();
  }, []);

  return (
    <div>
      {/* header */}
      {/* header place to here */}

      {/* main body */}
      {/* add all of the body content to the website application */}
      <main className="flex flex-col">
        {/* section  */}
        {/* show all of the introduction and some information about the website */}
        <section className="flex container mx-auto h-screen gap-20">
          {/* left side contain some illustration and images */}
          <div className="flex w-1/2 overflow-hidden">
            <img src={welcomeImage} alt="" className="object-fill" />
          </div>

          {/* right side about the application or website */}
          <div className="flex flex-col w-1/2 justify-center">
            {/* leading and desc */}
            <div className="flex flex-col gap-11">
              <h2 className="text-black font-bold text-6xl leading-tight">
                Get Latest update of modern article everyday
              </h2>
              <span className="text-xl text-gray-700 font-body">
                Amet minim mollit non deserunt ullamco est sit aliqua dolor do
                amet sint. Velit officia consequat.
              </span>
            </div>

            {/* actions */}
            <div className="flex mt-28">
              <button
                className="text-white bg-primary rounded-3xl h-14 px-10 transition-all duration-500 hover:-translate-y-3 hover:scale-110"
                onClick={() => onGetStartedClicked()}
              >
                Get Started
              </button>
            </div>
          </div>
        </section>
      </main>

      {/* footer */}
      {/* add some footer to the page */}
    </div>
  );
}
