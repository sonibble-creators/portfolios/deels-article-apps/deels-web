import { Link, NavLink } from "@remix-run/react";
import logoImage from "~/assets/images/logo.png";
import profileViewImage from "~/assets/images/illustration/profile-view.png";
import { Icon } from "@iconify/react";

/**
 * # SideNavMenuItemType
 *
 * type for side menu item
 */
export type SideNavMenuItemType = {
  label: string;
  link: string;
  icon: string;
};

/**
 * # SideNav
 *
 *
 * side navigation for the application
 * allow user to navigate the application into different pages
 *
 * @returns JSX.Element
 */
export default function SideNav(): JSX.Element {
  // define the default side nav
  // we can add and edit the side navigation based on the requirements
  const defaultSideNavMenus: SideNavMenuItemType[] = [
    { label: "Feed", link: "", icon: "bx:category" },
    { label: "Explore", link: "explore", icon: "carbon:explore" },
    { label: "saved", link: "saved", icon: "bi:bookmark-dash" },
  ];

  return (
    <div className="fixed inset-y-0 left-0 w-[257px] bg-white bg-opacity-50 backdrop-blur-3xl border-2 border-r-white z-[60]">
      <div className="flex flex-col h-full w-full py-5 px-1">
        {/* brand  */}
        {/* show the logo of the application or website */}
        {/* so allow the user to identify the brands */}
        <div className="flex gap-4 items-center mx-7 transition-all duration-700 hover:scale-105">
          <img src={logoImage} alt="" className="h-10 w-10 object-contain" />
          <h1 className="font-writing text-xl text-black">Deels</h1>
        </div>

        {/* the main menus of the application */}
        {/* showing the main menus of the application */}
        <div className="flex mt-24 mx-7">
          <ul className="list-none flex flex-col gap-6">
            {/* loop all of the side nav child */}
            {defaultSideNavMenus.map(
              ({ label, link, icon }: SideNavMenuItemType, index: number) => (
                <li
                  className="flex items-center py-2 transition-all duration-300 hover:-translate-x-2"
                  key={index}
                >
                  <NavLink
                    to={link}
                    className={({ isActive }) =>
                      `flex items-center gap-4 ${
                        isActive ? "text-primary" : "text-black"
                      }`
                    }
                  >
                    <Icon icon={icon} className="h-6 w-6" />
                    <span className="font-medium text-[15px]">{label}</span>
                  </NavLink>
                </li>
              )
            )}
          </ul>
        </div>

        {/* divider */}
        <div className="flex flex-col grow"></div>

        {/* bottom side menu */}
        {/* will contain some board with the profile actions */}
        <div className="flex relative flex-col bg-pink-500 rounded-2xl p-6 gap-6 shadow-2xl shadow-pink-300 transition-all duration-500 hover:-translate-y-4">
          <img src={profileViewImage} alt="" className="absolute -top-24" />

          <h4 className="text-white text-xl font-semibold font-heading mt-20">
            View on more space
          </h4>
          <Link
            to={"profile"}
            className="font-medium text-white px-6 py-3 rounded-full bg-white bg-opacity-30 text-center transition-all duration-300 hover:scale-110 hover:-translate-y-2"
          >
            View Profile
          </Link>
        </div>
      </div>
    </div>
  );
}
