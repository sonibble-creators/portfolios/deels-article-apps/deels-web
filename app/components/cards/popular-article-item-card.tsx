import { Icon } from "@iconify/react";

export default function PopularArticleItemCard(): JSX.Element {
  return (
    <div className="flex bg-white bg-opacity-50 p-3 rounded-3xl border-2 border-white flex-col w-3/12 transition-all duration-700 group hover:-translate-y-3 hover:scale-105">
      {/* images as cover of article */}
      <div className="flex overflow-hidden rounded-2xl h-[160px] group-hover:shadow-2xl group-hover:shadow-gray-200 transition-all duration-700">
        <img
          src="https://images.unsplash.com/photo-1508898578281-774ac4893c0c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80"
          alt=""
          className="object-cover h-full w-full"
        />
      </div>

      {/* title */}
      <div className="flex mt-5">
        <h2 className="text-lg font-semibold text-black leading-normal">
          The unseen of spending three years at Pixelgrade
        </h2>
      </div>

      {/* divider */}
      <div className="flex border-b border-b-gray-200 mt-5"></div>

      {/* writer or author of the article */}
      <div className="flex justify-between mt-5">
        {/* author */}
        <div className="flex gap-3">
          <div className="flex h-11 w-11 rounded-full overflow-hidden">
            <img
              src="https://images.unsplash.com/photo-1508898578281-774ac4893c0c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80"
              alt=""
              className="object-cover h-full w-full"
            />
          </div>

          <div className="flex flex-col gap-1">
            <span className="font-medium text-gray-700">Floyd Miles</span>
            <span className="text-sm text-gray-500">32 min ago</span>
          </div>
        </div>

        {/* save action */}
        <div className="flex h-10 w-10 rounded-full items-center justify-center transition-all duration-300 hover:scale-105">
          <Icon icon="bi:bookmark-dash" className="h-6 w-6 text-gray-700" />
        </div>
      </div>
    </div>
  );
}
