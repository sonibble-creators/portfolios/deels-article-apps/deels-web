import { Icon } from "@iconify/react";

/**
 * # ArticleItemCard
 *
 * teh artcile item card by default for most of needed
 *
 *
 * @returns JSX.Element
 */
export default function ArticleItemCard(): JSX.Element {
  return (
    <div className="flex bg-white bg-opacity-50 p-3 rounded-3xl border-2 border-white  transition-all duration-700 group hover:-translate-y-3 hover:scale-105 gap-4">
      {/* images as cover of article */}
      <div className="flex overflow-hidden rounded-2xl h-[140px] w-[140px] group-hover:shadow-2xl group-hover:shadow-gray-200 transition-all duration-700">
        <img
          src="https://images.unsplash.com/photo-1508898578281-774ac4893c0c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80"
          alt=""
          className="object-cover h-full w-full"
        />
      </div>

      {/* author */}
      <div className="flex flex-col gap-2 flex-grow">
        <span className="font-medium text-gray-700">By Sonibble</span>
        <h2 className="text-lg font-semibold text-black leading-normal">
          The unseen of spending three years at Pixelgrade
        </h2>
        <span className="text-sm text-gray-500 mt-3">32 min ago</span>
      </div>
    </div>
  );
}
