import { Icon } from "@iconify/react";
import { Link } from "@remix-run/react";
import noAvatarImage from "~/assets/images/illustration/no-avatar.png";

/**
 * # MainHeader
 *
 * the main header to use in every home page and teh other situations
 * @returns JSX.Element
 */
export default function MainHeader(): JSX.Element {
  return (
    <header className="flex w-full h-[75px] items-center fixed inset-x-0 top-0 bg-white bg-opacity-50 backdrop-blur-3xl border-b-2 border-b-white z-50">
      <div className="flex w-full mx-20 gap-10">
        {/* search  */}
        {/* enable the user to search and explore the article they needed */}
        <div className="flex h-12 rounded-xl bg-gray-100 w-3/12 ml-[300px] items-center px-4">
          <input
            type="text"
            className="ring-0 outline-none border-0 bg-transparent w-full h-full"
            placeholder="Search Everything"
          />
          <Icon icon="akar-icons:search" className="w-4 h4 text-gray-400" />
        </div>

        {/* actions  */}
        {/* something like notifications and more */}
        <div className="flex flex-grow"></div>
        <div className="flex items-center">
          {/* notification button */}
          {/* allow to check the latest and newest notification comming */}
          <div className="flex h-11 w-11 rounded-xl bg-pink-50 items-center justify-center relative transition-all duration-300 hover:scale-105">
            {/* lables */}
            <span className="h-3 w-3 rounded-full bg-pink-500 absolute -top-1 -right-1"></span>
            {/* icon */}
            <Icon
              icon="ic:round-notifications-none"
              className="h-6 w-6 text-pink-500 transform rotate-[15deg]"
            />
          </div>
        </div>

        {/* account menus */}
        {/* the user account and the other menus */}
        <div className="flex">
          <div className="flex group">
            {/* icon avatar */}
            {/* show the avatar */}
            <div className="flex h-12 w-12 rounded-full overflow-hidden relative border border-white transition-all duration-300 hover:-translate-y-1 hover:scale-105">
              <img
                src={noAvatarImage}
                alt=""
                className="object-cover w-full h-full rounded-full"
              />
            </div>

            {/* the account menus */}
            {/* allow to manage and show or hide the menu of account */}
            <div className="flex absolute w-[260px] bg-white bg-opacity-50 backdrop-blur-3xl rounded-3xl border-2 border-white p-5 top-16 -right-64 opacity-0 overflow-hidden group-hover:right-20 group-hover:opacity-100 transition-all duration-500">
              <div className="flex">
                <ul className="list-none">
                  <li className="flex items-center transition-all duration-300 hover:-translate-x-2">
                    <Link
                      to={""}
                      className="text-gray-700 gap-4 flex items-center"
                    >
                      <Icon icon="akar-icons:sign-out" className=" h-6 w-6" />
                      <span className="font-medium">Sign in</span>
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
}
