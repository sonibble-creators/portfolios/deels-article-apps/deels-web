import create from "zustand/react";

/**
 * # settingStore
 *
 * handle the all of state maneger about the setting
 * of the application, we need something to handle the setting of the website
 *
 */
export const settingStore = create((set, get) => ({}));
