import create from "zustand";
import { UserPayload } from "~/logic/model/user/user-payload";

/**
 * # UserStoreType
 *
 * type checking for the user store
 */
export type UserStoreType = {
  user?: UserPayload;
};

/**
 * # userStore
 *
 * the store to handle all of user data
 * the basic, info, and some summary about the user
 *
 */
export const userStore = create<UserStoreType>((set, get) => ({
  // the user
  // all information for basic user, data , permission, role are inside the user
  // set the basic data into undefined
  user: undefined,
}));
