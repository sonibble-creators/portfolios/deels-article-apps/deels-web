/**
 * # Database Collection Key
 *
 * all database collection key will be defined here
 * this will enable to keep all of consistently
 *
 */

export const USER_COLLECTION_KEY: string = "users";
