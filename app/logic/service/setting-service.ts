/**
 * # SettingService
 *
 * handle all of the setting logic for the application
 * we need something to handle all of the configuration and also about the token, session and more
 */
export default class SettingService {
  constructor() {}

  // define all of the key
  // that we needed
  private readonly GET_STARTED_KEY: string = "get_started";

  /**
   * ## isAlreadyGetStartedPassed
   *
   * check and validate if the user already passed the get started
   *
   * @returns boolean
   */
  async isAlreadyGetStartedPassed(): Promise<boolean> {
    // firt we need to check the storage about this action
    const result = localStorage.getItem(this.GET_STARTED_KEY);

    // check and validate the result
    if (!result) {
      // opps, the result, record not found
      // we need to tell that the user of client not been get started
      // and need to show the get started page view
      return false;
    }

    // the result, record found
    // we need to tell that the user of client been get started
    return true;
  }

  /**
   * ## setGetStartedPassed
   *
   * set the user to already passed the get started view
   */
  async setGetStartedPassed(): Promise<void> {
    // first we need to set the storage about this action
    localStorage.setItem(this.GET_STARTED_KEY, "true");
  }
}
