import { GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import {
  collection,
  doc,
  getDocs,
  limit,
  query,
  setDoc,
  where,
} from "firebase/firestore";
import { authentication, database } from "../connection/firebase";
import { UserSignWithGoogleInput } from "../model/user/user-input";
import { UserPayload, UserRole, UserStatus } from "../model/user/user-payload";
import { v4 } from "uuid";
import { userStore } from "../store/user-store";
import { USER_COLLECTION_KEY } from "../data/database-collection-key";
import { USER_LOCAL_KEY } from "../data/local-key";

/**
 * # UserService
 *
 * the service to handle all of the user logic
 * include some authentication, role, permission and more
 */
export default class UserService {
  constructor() {}

  /**
   * ## signUpWithGoogle
   *
   * enable the user to sign up using the google and
   * save all basic data into the database
   *
   * @returns [string | undefined, UserPayload | undefined]
   */
  readonly signUpWithGoogle: () => Promise<
    [string | undefined, UserPayload | undefined]
  > = async () => {
    // before we go, we need to prepare some data for the provider
    // of course it's google
    const provider = new GoogleAuthProvider();

    // now we need to sign using the popup method
    const result = await signInWithPopup(authentication, provider);

    // we need to check and validate the result to reduce the risk of error
    if (!result) {
      // opps something error found. We need to return some message
      return [
        "Opps, something error when trying to sign up using google, Please use other method or try again.",
        undefined,
      ];
    }

    // nice the sign using google is complete
    // now we need to get the user data from the users
    const credential = GoogleAuthProvider.credentialFromResult(result);
    const googleUser = result.user;

    // we need to define and predefined the username for the first signup
    // we need to replace all of the white space from the name and bring into the username
    const predefinedUsername =
      googleUser.displayName?.replace(/\s+/g, "") || "".toLowerCase();

    const input: UserSignWithGoogleInput = {
      id: v4(), //  use the uuid for generate and use the id
      username: predefinedUsername,
      email: googleUser.email || "",
      fullName: googleUser.displayName || "",
      avatar: googleUser.photoURL || undefined,
      role: UserRole.USER,
      status: UserStatus.ACTIVE,
      updatedAt: new Date(),
      createdAt: new Date(),
    };

    // we need to check the same email register in database
    // we want to avoid the mail duplication
    const userRef = collection(database, USER_COLLECTION_KEY);
    const queryResult = query(
      userRef,
      where("email", "==", googleUser.email),
      limit(1)
    );
    const snapshot = await getDocs(queryResult);
    if (snapshot.empty) {
      // we're going need to create one
      // now we need to store data into the database to save the whole data
      // we need to define the whole data to user

      // defien some reference to database
      const docRef = doc(database, USER_COLLECTION_KEY, input.id);

      // now, let's go to save the data
      await setDoc(docRef, input);

      // after the data saved, we need to bring it into the user store and become state
      userStore.setState({ user: input });

      const payload = input as UserPayload;

      // don't forget to save the user data into the local storage
      localStorage.setItem(USER_LOCAL_KEY, JSON.stringify(payload));

      // and now  we need to return the data
      // without error message
      return [undefined, payload];
    }

    // the data already exist
    // we just need to get it all
    return [undefined, input as UserPayload];
  };

  /**
   * ## checkAccountExists
   *
   * we're going to check if the accoun is already sign in or not
   * so we can enable to load all user, and then passed to the store
   *
   * @returns boolean
   */
  readonly checkAccountExists: () => Promise<boolean> = async () => {
    // we need to check from the local storage that we save before
    const result = localStorage.getItem(USER_LOCAL_KEY);

    // we need to check and validate the result
    if (!result) {
      // opps, no uer found.
      // this mean we need to sign in again to get the current user
      return false;
    }

    return true;
  };

  /**
   * ## loadUser
   *
   * enable to load tehe current user data and all of
   * the specify data for the user
   */
  readonly loadUser: () => Promise<void> = async () => {
    // we need to get the current user from the sign in
    const result = localStorage.getItem(USER_LOCAL_KEY);

    // before we going we need to validate
    if (result) {
      const currentUser = JSON.parse(result) as UserPayload;
      // nice the current user found
      // now we need to load all data from the database

      // define the query
      const userRef = collection(database, USER_COLLECTION_KEY);
      const queryResult = query(
        userRef,
        where("email", "==", currentUser.email),
        limit(1)
      );

      // start execute and find the data
      const snapshot = await getDocs(queryResult);
      snapshot.forEach((doc) => {
        // now we need to store all of the data
        const data = doc.data() as UserPayload;
        userStore.setState({ user: data });
      });

      // nice all done
    }
  };
}
