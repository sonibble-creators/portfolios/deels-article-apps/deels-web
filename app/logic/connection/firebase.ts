/**
 * # Firebase Connection configuration File
 *
 * the configuration file for the firebase connection
 * the connection through to the firebase
 * something like database, firestore, authentication, analythics, etc
 *
 */
import { FirebaseApp, FirebaseOptions, initializeApp } from "firebase/app";
import { Firestore, getFirestore } from "firebase/firestore";
import { Auth, getAuth } from "firebase/auth";
import { FirebaseStorage, getStorage } from "firebase/storage";

/**
 * ## firebaseConfig
 *
 * Allow to manage and some options for the configuration for
 * the firebase connection
 *
 * The configuration is just the setting about the preferences and some
 * detail info
 */
const firebaseConfig: FirebaseOptions = {
  apiKey: "AIzaSyAvp7NKcJ3c2B2I_NWMu6KruMhF9Kdupig",
  authDomain: "deels-article-app-sonibble.firebaseapp.com",
  projectId: "deels-article-app-sonibble",
  storageBucket: "deels-article-app-sonibble.appspot.com",
  messagingSenderId: "215153686663",
  appId: "1:215153686663:web:615da3344750b809d2e022",
  measurementId: "G-HJQ98SEGTY",
};

// before we can use the firebase service,
// we need to init the app followed with the configuration and some options
const app: FirebaseApp = initializeApp(firebaseConfig);

// now we need to defines all of the service for all what
// we need

/**
 * # database
 *
 * allow to manage the collection service using the firestore
 * database
 */
const database: Firestore = getFirestore(app);

/**
 * # Auth
 *
 * the auth data and service to handle all
 * of the authentication
 */
const authentication: Auth = getAuth(app);

/**
 * # Storage
 *
 * the storage to store, and upload all of the assets needed
 */
const storage: FirebaseStorage = getStorage(app);

// now we need to export all we need
// we just export what we need
export { database, authentication, storage };
