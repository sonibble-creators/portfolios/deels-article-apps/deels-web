/**
 * # Userpayload
 *
 * payload to handle the user data and some information
 */
export interface UserPayload {
  id: string;
  fullName: string;
  username: string;
  email: string;
  cover?: string;
  avatar?: string;
  createdAt: Date;
  updatedAt: Date;
  role?: UserRole;
  permission?: UserPermission;
  status: UserStatus;
}

/**
 * # UserStatus
 *
 * status model for the user
 *
 */
export enum UserStatus {
  ACTIVE = "active",
  DISABLE = "disable",
  DELETED = "deleted",
}

/**
 * # UserRole
 *
 * role model for the users
 * some role needed like admin, user, and the creators
 */
export enum UserRole {
  ADMIN = "admin",
  USER = "user",
  CREATOR = "creator",
}

/**
 * # UserPermission
 *
 * the permision model for the users
 */
export interface UserPermission {
  name: string;
  scope: string[];
}
