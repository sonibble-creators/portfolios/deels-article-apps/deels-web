import { UserPermission, UserRole, UserStatus } from "./user-payload";

/**
 * # UserSignWithGoogleInput
 *
 * input to sign in using google to the users
 * some data needed like id, fullname, username, email, and so on
 */
export interface UserSignWithGoogleInput {
  id: string;
  fullName: string;
  username: string;
  email: string;
  cover?: string;
  avatar?: string;
  createdAt: Date;
  updatedAt: Date;
  role?: UserRole;
  permission?: UserPermission;
  status: UserStatus;
}
