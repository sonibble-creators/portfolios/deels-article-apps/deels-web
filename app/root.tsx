import type { LinksFunction, MetaFunction } from "@remix-run/node";
import {
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
} from "@remix-run/react";
import appStyle from "~/styles/app.css";
import faviconImage from "~/assets/images/favicon.png";

export const meta: MetaFunction = () => ({
  charset: "utf-8",
  title: "Deels - Browse thousand free article for your toxic life",
  viewport: "width=device-width,initial-scale=1",
});

export const links: LinksFunction = () => {
  return [
    { rel: "stylesheet", href: appStyle },
    { rel: "icon", type: "image/png", href: faviconImage },
  ];
};

export default function App() {
  return (
    <html lang="en">
      <head>
        {/* add meta tags from the meta function */}
        <Meta />

        {/* add the links from the links function */}
        <Links />
      </head>
      <body className="font-body text-base font-normal text-black leading-normal bg-primary-background selection:bg-pink-500 selection:text-white">
        {/* the child component that show as a routes  */}
        {/* maybe come with many of stack and even come with nested routes */}
        <Outlet />

        {/* other components */}
        <ScrollRestoration />
        <Scripts />
        <LiveReload />
      </body>
    </html>
  );
}
